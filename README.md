# React Native - Session 2

This project is a series of 3 sessions that show how to build a mobile app using [React Native][1] and [Expo][2].

Session 2 introduces an example about how to navigate between two screens using [React Navigation][3].

[1]: https://facebook.github.io/react-native/
[2]: https://expo.io/
[3]: https://reactnavigation.org