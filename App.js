import React, { Component } from 'react';

import RootStack from './src/routers/RootStack';

class App extends Component {
  render() {
    return (
      <RootStack/>
    );
  }
}

export default App;
