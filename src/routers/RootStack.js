import { createStackNavigator } from 'react-navigation';

import HomeScreen from 'src/screens/Home';
import LearnScreen from 'src/screens/Learn';

export default createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
    },
    LearnScreen: {
      screen: LearnScreen,
    },
  },
  {
    initialRouteName: 'HomeScreen',
    navigationOptions: {
      title: 'LearnFirst'
    }
  }
);
