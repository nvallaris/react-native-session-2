import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  answerContainer: {
    borderWidth: 0.5,
    borderColor: '#000',
  },
  questionText: {
    borderBottomWidth:  0.5,
    borderBottomColor: '#000',
    padding: 10,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  answerText: {
    padding: 10
  }
});
