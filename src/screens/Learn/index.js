import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import styles from './styles';

class LearnScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.answerContainer}>
          <Text style={styles.questionText}>What is React Native?</Text>
          <Text style={styles.answerText}>React Native is a JavaScript framework for writing real, natively rendering mobile applications for iOS and Android.</Text>
        </View>
      </View>
    );
  }
}

export default LearnScreen;
