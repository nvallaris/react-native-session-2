import React, { Component } from 'react';
import { Button, View } from 'react-native';

import styles from './styles';

class HomeScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
          title='Start Learning'
          onPress={() => this.props.navigation.navigate('LearnScreen')}
        />
      </View>
    );
  }
}

export default HomeScreen;
 